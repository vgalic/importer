from django.urls import path
from django.contrib import admin

from . import views

urlpatterns = [
    path('', views.index, name='index'),
]

admin.site.site_header = "INTIS importer"
admin.site.site_title = "INTIS importer"
admin.site.index_title = "INTIS importer"
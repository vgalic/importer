from django.db import models
from django.core.exceptions import ValidationError

STATUS_CHOICES = (
    ('enabled', 'enabled'),
    ('disabled', 'disabled'),
    ('deleted', 'deleted'),
)

class Device(models.Model):
    name = models.CharField(max_length=32)
    description = models.TextField()
    code = models.CharField(max_length=30, unique=True)
    date_created = models.DateTimeField('date created', auto_now_add=True, )
    date_updated = models.DateTimeField('date updated', auto_now=True,)
    expire_date = models.DateTimeField('expire date',  blank=True, null=True)
    status = models.CharField(max_length=8, choices=STATUS_CHOICES, blank=True, null=True)

class Content(models.Model):
    name = models.CharField(max_length=32)
    description = models.TextField()
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    date_created = models.DateTimeField('date created', auto_now_add=True)
    date_updated = models.DateTimeField('date updated', auto_now=True)
    expire_date = models.DateTimeField('expire date')
    status = models.CharField(max_length=8, choices=STATUS_CHOICES)

class ImportSettings(models.Model):
    csv_file_path = models.TextField()
    csv_delimiter = models.CharField(max_length=1, default = ',')

    def save(self, *args, **kwargs):
        if ImportSettings.objects.exists() and not self.pk:
        # if you'll not check for self.pk 
        # then error will also raised in update of exists model
            raise ValidationError('There is can be only one ImportSettings instance')
        return super(ImportSettings, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Import settings"



  

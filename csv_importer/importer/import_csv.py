import csv
import os
from vladiate import Vlad
from vladiate.validators import UniqueValidator, SetValidator
from vladiate.inputs import LocalFile
import datetime
from dateutil import parser
import logging
from .models import ImportSettings, Device, Content

logger = logging.getLogger(__name__)


def RepresentsInt(int_text):
    try:
        int(int_text)
        return True
    except ValueError:
        return False


def RepresentsDateTime(date_text):
    try:
        parser.parse(date_text)
        return True
    except ValueError:
        return False


valid_statuses = ['enabled', 'disabled', 'deleted']


def RepresentsValidStatus(status_text):
    try:
        if status_text is None:
            return False

        status_text = status_text.strip()
        if status_text in valid_statuses:
            return True
        else:
            return False
    except ValueError:
        return False

#validates devices.csv data and writes errors to log file
def validateDevices(self, delimiter, path):
    device_column_count = 6
    devices_for_insert = []
    with open(path) as f:
        reader = csv.DictReader(f, fieldnames=[
                                'id', 'name', 'description', 'code', 'expire_date', 'status'], delimiter=delimiter, quotechar='"')
        for row in reader:
            rowOk = True
            if len(row) != device_column_count:
                logger.warning(
                    '%s -> id:%s wrong number of columns', path, row['id'])
                rowOk = False
            else:
                if not RepresentsInt(row['id']):
                    logger.warning(
                        '%s -> id:%s id is not valid integer', path, row['id'])
                    rowOk = False
                if len(row['name']) > 32:
                    logger.warning(
                        '%s -> id:%s name longer than 32', path, row['id'])
                    rowOk = False
                if len(row['code']) > 30:
                    logger.warning(
                        '%s -> id:%s code longer than 30', path, row['id'])
                    rowOk = False
                if not RepresentsDateTime(row['expire_date']):
                    logger.warning(
                        '%s -> id:%s expire date in wrong format ', path, row['id'])
                    rowOk = False
                if not RepresentsValidStatus(row['status']):
                    logger.warning(
                        '%s -> id:%s status not in(enabled, disabled, deleted) ', path, row['id'])
                    rowOk = False

            if rowOk == True:
                logger.info('%s -> id:%s row OK ', path, row['id'])
                devices_for_insert.append(row)

    return devices_for_insert

#validates content.csv data and writes errors to log file
def validateContent(self, delimiter, path):
    content_column_count = 6
    content_for_insert = []
    with open(path) as f:
        reader = csv.DictReader(f, fieldnames=[
                                'id', 'name', 'description', 'device', 'expire_date', 'status'], delimiter=delimiter, quotechar='"')
        for row in reader:
            rowOk = True
            if len(row) != content_column_count:
                logger.warning(
                    '%s -> id: %s wrong number of columns', path, row['id'])
                rowOk = False
            else:
                if not RepresentsInt(row['id']):
                    logger.warning(
                        '%s -> id:%s id is not valid integer', path, row['id'])
                    rowOk = False
                if len(row['name']) > 100:
                    logger.warning(
                        '%s -> id:%s name longer than 100', path, row['id'])
                    rowOk = False
                if not RepresentsInt(row['device']):
                    logger.warning(
                        '%s -> id:%s device id is not valid integer', path, row['id'])
                    rowOk = False
                if not RepresentsDateTime(row['expire_date']):
                    logger.warning(
                        '%s -> id:%s expire date in wrong format ', path, row['id'])
                    rowOk = False
                if not RepresentsValidStatus(row['status']):
                    logger.warning(
                        '%s -> id:%s status not in(enabled, disabled, deleted) ', path, row['id'])
                    rowOk = False

            if rowOk == True:
                logger.info('%s -> id:%s row OK ', path, row['id'])
                content_for_insert.append(row)
    return content_for_insert

#rename files after sucesfull import
def renameImportedFile(file):
    try:
        time_sufix = datetime.datetime.now().strftime("%Y%m%d_ %H%M%S")
        new_file_name = file + time_sufix
        os.rename(file, new_file_name)
    except Exception as e:
        logger.error('File rename error: %s', e)

#save devices data to DB
def saveDevices(self, devices):
    try:
        for row in devices:
            Device.objects.update_or_create(
                id=row['id'].strip(), 
                code=row['code'].strip(), 
                
                defaults={'name': row['name'].strip(),
                          'description': row['description'].strip(),
                          'expire_date': row['expire_date'].strip(),
                          'status': row['status'].strip(),
                },
            )
    except Exception as e:
        logger.error('Devices save error: %s', e)

#save devices data to DB
def saveContent(self, devices):
    try:
        for row in devices:
            device = Device.objects.get(pk=row['device'].strip())
            Content.objects.update_or_create(
                id=row['id'].strip(), 

                defaults={'name': row['name'].strip(),
                          'description': row['description'].strip(),
                          'device': device,
                          'expire_date': row['expire_date'].strip(),
                          'status': row['status'].strip(),
                },
                description=row['description'].strip(), 
                device=device, 
                expire_date=row['expire_date'].strip(), 
                status=row['status'].strip()
            )
    except Exception as e:
        logger.error('Content save error: %s', e)

def validateAndImportAllFiles(self):
    try:
        settings = ImportSettings.objects.all().values('csv_file_path', 'csv_delimiter')
        delimiter = settings[0]["csv_delimiter"].strip()
        file_path = settings[0]["csv_file_path"].strip()

        devices_path = os.path.join(file_path, 'devices.csv')
        if not os.path.isfile(devices_path):
            logger.error('File  %s does not exist', devices_path)
            return False

        content_path = os.path.join(file_path, 'content.csv')
        if not os.path.isfile(content_path):
            logger.error('File  %s does not exist', content_path)
            return False

        devices = validateDevices(self, delimiter, devices_path)
        content = validateContent(self, delimiter, content_path)

        saveDevices(self, devices)
        saveContent(self, content)

        renameImportedFile(devices_path)
        renameImportedFile(content_path)
        return True
    except Exception as e:
        logger.error('Import error: %s', e)
        return False

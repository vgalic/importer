from django.contrib import admin
from django.conf.urls import url
from . import views
from django.urls import path
from django.http import HttpResponseRedirect
from .models import ImportSettings
from . import import_csv
from .models import ImportSettings, Device, Content
from django.contrib import messages

import logging

logger = logging.getLogger(__name__)


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "description",
                    "code", "expire_date", "status")
    list_filter = ("id", "name", "description",
                   "code", "expire_date", "status")


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "description",
                    "device", "expire_date", "status")
    list_filter = ("id", "name", "description",
                   "device", "expire_date", "status")


@admin.register(ImportSettings)
class ImportSettingsAdmin(admin.ModelAdmin):
    list_display = ("csv_file_path", "csv_delimiter")
    change_list_template = "importer/importsettings_changelist.html"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path("runimport/", self.runimport),
        ]
        return my_urls + urls

    #validate CSV data and save to DB
    def runimport(self, request):
        try:
            if import_csv.validateAndImportAllFiles(self):
                self.message_user(request, "CSV import finished")
                return HttpResponseRedirect("../")
            else:
                self.message_user(
                    request, "Something went wrong, please check the log", level=messages.ERROR)
                return HttpResponseRedirect("../")
        except:
            self.message_user(
                request, "Something went wrong, please check the log", level=messages.ERROR)
            return HttpResponseRedirect("../")
